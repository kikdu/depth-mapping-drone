using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR;
using System.IO;

public class ControllerBirdViewHandlerScript : MonoBehaviour
{
    // a reference to the action
    public SteamVR_Action_Vector2 joystickLeft;
    public SteamVR_Action_Vector2 joystickRight;
    public SteamVR_Action_Boolean triggerLeft;
    public SteamVR_Action_Boolean triggerRight;
    public SteamVR_Action_Boolean aRightButton;
    public SteamVR_Action_Boolean xButton;
    public SteamVR_Input_Sources leftHand;
    public SteamVR_Input_Sources rightHand;
    public GameObject goContainer;
    private bool leftTriggerIsDown;
    private bool rightTriggerIsDown;
    public bool deleteFiles;
    
    // Start is called before the first frame update
    void Start()
    {
        leftTriggerIsDown = false;
        triggerLeft.AddOnStateDownListener(TriggerDownLeft, leftHand);
        triggerLeft.AddOnStateUpListener(TriggerUpLeft, leftHand);
        triggerRight.AddOnStateDownListener(TriggerDownRight, rightHand);
        triggerRight.AddOnStateUpListener(TriggerUpRight, rightHand);
        aRightButton.AddOnStateDownListener(ARightButton, rightHand);
        xButton.AddOnStateDownListener(XButton, leftHand);
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 newLeftJoystickPosition = joystickLeft.GetAxis(SteamVR_Input_Sources.Any);
        Vector2 newRightJoystickPosition = joystickRight.GetAxis(SteamVR_Input_Sources.Any);
        
        //goContainer.transform.position = goContainer.transform.position + new Vector3(-(newLeftJoystickPosition.x/10), newLeftJoystickPosition.y/10, newRightJoystickPosition.x/10);

        
        goContainer.transform.Translate(new Vector3(-newLeftJoystickPosition.x/10, newLeftJoystickPosition.y/10, newRightJoystickPosition.x/10));
        
        if(leftTriggerIsDown)
        {
            goContainer.transform.Rotate(new Vector3(0, -5, 0));
        }

        if(rightTriggerIsDown)
        {
            goContainer.transform.Rotate(new Vector3(0, 5, 0));
        }
    }

    public void TriggerDownLeft(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        leftTriggerIsDown = true;
    }

    public void TriggerUpLeft(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        leftTriggerIsDown = false;
    }

    public void TriggerDownRight(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        rightTriggerIsDown = true;
    }

    public void TriggerUpRight(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        rightTriggerIsDown = false;
    }

    public void ARightButton(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        goContainer.transform.position = new Vector3(0,0,0);
        goContainer.transform.rotation = Quaternion.Euler(new Vector3(0,0,0));
    }

    public void XButton(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        if(SceneManager.GetActiveScene().name != "BirdView") return;
        if(deleteFiles)
        {
            DeleteFiles();
        }
        SceneManager.LoadScene("Planetarium");
    }

    private void DeleteFiles()
    {
        string sAssetFolderPath = "Assets/SpatialMappings";
        string[] aux = sAssetFolderPath.Split(new char[] { '/' });
        string onlyFolderPath = aux[0] + "/" + aux[1] + "/";
        string[] aFilePaths = Directory.GetFiles(onlyFolderPath);
        foreach(string sFilePath in aFilePaths)
        {
            File.Delete(sFilePath);
        }
    }

    void OnDestroy()
    {
        if(deleteFiles)
        {
            DeleteFiles();
        }
    }
}
