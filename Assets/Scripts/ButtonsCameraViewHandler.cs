using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.IO;

public class ButtonsCameraViewHandler : MonoBehaviour
{
    public Button birdViewButton;
    public ZEDManager zedManager;
    private bool switchingSceneWanted;
    private string filename;
    public GameObject loader;
    
    // Start is called before the first frame update
    void Start()
    {
        switchingSceneWanted = false;
        filename = null;
        loader.SetActive(false);

        birdViewButton.onClick.AddListener(()=> {
            filename = "Assets/SpatialMappings/ZEDMesh" + ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds() + ".obj";
            zedManager.StopSpatialMapping(filename);
            switchingSceneWanted = true;
        });
    }

    // Update is called once per frame
    void Update()
    {
        if(switchingSceneWanted && CheckNewFilePresence())
        {
            SceneManager.LoadScene("BirdView");
        }

        if(switchingSceneWanted)
        {
            loader.SetActive(true);
        }
    }

    private bool CheckNewFilePresence()
    {
        string sAssetFolderPath = "Assets/SpatialMappings";
        string[] aux = sAssetFolderPath.Split(new char[] { '/' });
        string onlyFolderPath = aux[0] + "/" + aux[1] + "/";
        string[] aFilePaths = Directory.GetFiles(onlyFolderPath);
        foreach(string sFilePath in aFilePaths)
        {
            if (sFilePath == filename)
            {
                return true;
            }
        }
        return false;
    }
}
