using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR;
using System.IO;
using System;

public class ControllerCameraViewHandlerScript : MonoBehaviour
{
    public SteamVR_Action_Boolean yButton;
    public SteamVR_Input_Sources leftHand;
    public ZEDManager zedManager;
    private bool switchingSceneWanted;
    private string filename;
    public GameObject zedRigStereo;
    public GameObject cameraDuringTransition;
    public GameObject loaderAnimation;
    public GameObject textLoading;
    // Start is called before the first frame update
    void Start()
    {
        switchingSceneWanted = false;
        filename = null;
        cameraDuringTransition.SetActive(false);
        loaderAnimation.SetActive(false);
        textLoading.SetActive(false);
        yButton.AddOnStateDownListener(YButton, leftHand);
    }

    // Update is called once per frame
    void Update()
    {
        if(switchingSceneWanted && CheckNewFilePresence())
        {
            DestroyImmediate(loaderAnimation);
            textLoading.SetActive(false);
            switchingSceneWanted = false;
            Invoke("SwitchScene", 1.0f);
            
        }

        if(switchingSceneWanted)
        {
            if(loaderAnimation.activeSelf)
            {
                loaderAnimation.transform.Rotate(new Vector3(0, -5, 5));
            }
        }
    }
    public void YButton(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        if(zedManager == null || !zedManager.IsMappingRunning || SceneManager.GetActiveScene().name != "Planetarium") return;
        cameraDuringTransition.SetActive(true);
        loaderAnimation.SetActive(true);
        textLoading.SetActive(true);
        switchingSceneWanted = true;
        filename = "Assets/SpatialMappings/ZEDMesh" + ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds() + ".obj";
        zedManager.StopSpatialMapping(filename);
    }

    private bool CheckNewFilePresence()
    {
        string sAssetFolderPath = "Assets/SpatialMappings";
        string[] aux = sAssetFolderPath.Split(new char[] { '/' });
        string onlyFolderPath = aux[0] + "/" + aux[1] + "/";
        string[] aFilePaths = Directory.GetFiles(onlyFolderPath);
        foreach(string sFilePath in aFilePaths)
        {
            if (sFilePath == filename)
            {
                return true;
            }
        }
        return false;
    }
    private void SwitchScene()
    {
        SceneManager.LoadScene("BirdView");
    }
}
