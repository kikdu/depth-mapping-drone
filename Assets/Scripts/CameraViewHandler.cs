using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CameraViewHandler : MonoBehaviour
{
    public ZEDManager zedManager;
    private bool mappingStarted;
    public float timeToWaitBeforeStartMapping;
    private float timeWhenInitialized;

    // Start is called before the first frame update
    void Start()
    {
        mappingStarted = false;
        timeWhenInitialized = 0f;
        if(timeToWaitBeforeStartMapping == 0)
        {
            timeToWaitBeforeStartMapping = 2;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(!mappingStarted && zedManager.zedCamera.IsCameraReady && zedManager.IsZEDReady && timeWhenInitialized == 0f)
        {
            timeWhenInitialized = Time.time;
        }

        if(Time.time-timeWhenInitialized > timeToWaitBeforeStartMapping && timeWhenInitialized != 0f)
        {
            zedManager.StartSpatialMapping();
            mappingStarted = true;
        }
    }
    
}
