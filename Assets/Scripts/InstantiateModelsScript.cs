using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEditor;

public class InstantiateModelsScript : MonoBehaviour
{
    private List<UnityEngine.GameObject> instantiatedModels;
    public GameObject goContainer;
    public GameObject cameraVR;
    
    // Start is called before the first frame update
    void Start()
    {
        instantiatedModels = new List<UnityEngine.GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        ScanFolder();
    }

    private void ScanFolder()
    {
        string sAssetFolderPath = "Assets/SpatialMappings";
        string[] aux = sAssetFolderPath.Split(new char[] { '/' });
        string onlyFolderPath = aux[0] + "/" + aux[1] + "/";
        string[] aFilePaths = Directory.GetFiles(onlyFolderPath);

        int countOfModels = 0;
        
        foreach(string sFilePath in aFilePaths)
        {
            if (Path.GetExtension(sFilePath) == ".obj")
            {
                countOfModels++;
            }
        }

        if(countOfModels > instantiatedModels.Count)
        {
            InstantiateModels(aFilePaths);
        }
    }

    private void InstantiateModels(string[] files)
    {
        DestroyAllModels();
        foreach(string sFilePath in files)
        {
            if (Path.GetExtension(sFilePath) == ".obj")
            {
                UnityEngine.GameObject objAsset = (GameObject) AssetDatabase.LoadAssetAtPath(sFilePath, typeof(UnityEngine.GameObject));
                UnityEngine.GameObject gameObject = Instantiate(objAsset, new Vector3(-562.09f, -0.6f, 925.71f), Quaternion.Euler(new Vector3(0,0,0)));
                instantiatedModels.Add(gameObject);
                ((GameObject)gameObject).transform.SetParent(goContainer.transform);
            }
        }
    }

    private void DestroyAllModels()
    {
        AssetDatabase.Refresh();
        AssetDatabase.ResetCacheServerReconnectTimer();
        foreach(UnityEngine.GameObject go in instantiatedModels)
        {
            Destroy(go);
        }
        instantiatedModels.Clear();
    }
}
